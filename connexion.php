<?php 

try{
include ('parametre.php');
$bdd = new PDO('mysql:host=sqletud.u-pem.fr;dbname=kgomes_db', $user, $pass, array(PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION));
$bdd->exec("SET CHARACTER SET utf8");
$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$bdd->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
}
catch(Exception $e ){
	die('Connexion à la base de données impossible!'.$e->getMessage());
}
 
 ?>
