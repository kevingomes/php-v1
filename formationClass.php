<?php

class Formation {
	//JSON Ecoles
	public $uai; //etablissement
	public $nomEtab; //etablissement_lib
	public $typeDipl; //dn_de_lib
	public $cycleUniv; //cursus_lmd_lib
	public $regroupDipl; //diplome_rgp
	public $dipl; //diplome_lib
	public $typeDiplForm; //typ_diplome_lib
	public $libDipl; //libelle_intitule_1
	public $nivDipl; //niveau_lib
	public $grandDisc; //gd_disciscipline_lib
	public $disc; //discipline_lib
	public $sectDisc; //sect_disciplinaire_lib
	public $nbEtudi; //effectif
	public $nbHomme; //hommes
	public $nbFemme; //femmes

	public function __construct($uai, $nomEtab, $typeDipl, $cycleUniv, $regroupDipl, $dipl, $typeDiplForm, $libDipl, $nivDipl, $grandDisc, $disc, $sectDisc, $nbEtudi, $nbHomme, $nbFemme) {

		$this->uai = $uai;
		$this->nomEtab = $nomEtab;
		$this->typeDipl = $typeDipl;
		$this->cycleUniv = $cycleUniv;
		$this->regroupDipl = $regroupDipl;
		$this->dipl = $dipl;
		$this->typeDiplForm = $typeDiplForm;
		$this->libDipl = $libDipl;
		$this->nivDipl = $nivDipl;
		$this->grandDisc = $grandDisc;
		$this->disc = $disc;
		$this->sectDisc = $sectDisc;
		if($nbEtudi == null){
			$this->nbEtudi = 0;
		} else {
			$this->nbEtudi = $nbEtudi;
		}
		if($nbHomme == null){
			$this->nbHomme = 0;
		} else {
			$this->nbHomme = $nbHomme;
		}
		if($nbFemme == null){
			$this->nbFemme = 0;
		} else {
			$this->nbFemme = $nbFemme;
		}
	}
}

?>