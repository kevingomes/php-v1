<div id="mapid" style="width: 1480px; height: 810px; position: relative; outline: medium none currentcolor;" class="leaflet-container leaflet-touch leaflet-fade-anim leaflet-grab leaflet-touch-drag leaflet-touch-zoom" tabindex="0">
	<div class="leaflet-pane leaflet-map-pane" style="transform: translate3d(0px, 0px, 0px);">
		<div class="leaflet-pane leaflet-tile-pane">
			<div class="leaflet-layer " style="z-index: 1; opacity: 1;">
				<div class="leaflet-tile-container leaflet-zoom-animated" style="z-index: 16; transform: translate3d(126px, 192px, 0px) scale(0.25);"></div>
					<div class="leaflet-tile-container leaflet-zoom-animated" style="z-index: 18; transform: translate3d(0px, 0px, 0px) scale(1);">
						<img alt="" role="presentation" src="https://api.tiles.mapbox.com/v4/mapbox.streets/11/1023/680.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw" class="leaflet-tile leaflet-tile-loaded" style="width: 256px; height: 256px; transform: translate3d(196px, -73px, 0px); opacity: 1;">
						<img alt="" role="presentation" src="https://api.tiles.mapbox.com/v4/mapbox.streets/11/1023/681.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw" class="leaflet-tile leaflet-tile-loaded" style="width: 256px; height: 256px; transform: translate3d(196px, 183px, 0px); opacity: 1;">
						<img alt="" role="presentation" src="https://api.tiles.mapbox.com/v4/mapbox.streets/11/1022/680.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw" class="leaflet-tile leaflet-tile-loaded" style="width: 256px; height: 256px; transform: translate3d(-60px, -73px, 0px); opacity: 1;">
						<img alt="" role="presentation" src="https://api.tiles.mapbox.com/v4/mapbox.streets/11/1024/680.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw" class="leaflet-tile leaflet-tile-loaded" style="width: 256px; height: 256px; transform: translate3d(452px, -73px, 0px); opacity: 1;">
						<img alt="" role="presentation" src="https://api.tiles.mapbox.com/v4/mapbox.streets/11/1022/681.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw" class="leaflet-tile leaflet-tile-loaded" style="width: 256px; height: 256px; transform: translate3d(-60px, 183px, 0px); opacity: 1;">
						<img alt="" role="presentation" src="https://api.tiles.mapbox.com/v4/mapbox.streets/11/1024/681.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw" class="leaflet-tile leaflet-tile-loaded" style="width: 256px; height: 256px; transform: translate3d(452px, 183px, 0px); opacity: 1;">
					</div>
				</div>
			</div>
			<div class="leaflet-pane leaflet-shadow-pane"></div>
			<div class="leaflet-pane leaflet-overlay-pane"></div>
			<div class="leaflet-pane leaflet-marker-pane"></div>
			<div class="leaflet-pane leaflet-tooltip-pane"></div>
			<div class="leaflet-pane leaflet-popup-pane"></div>
			<div class="leaflet-proxy leaflet-zoom-animated" style="transform: translate3d(261992px, 174353px, 0px) scale(1024);"></div>
		</div>
		<div class="leaflet-control-container">
		<div class="leaflet-top leaflet-left">
			<div class="leaflet-control-zoom leaflet-bar leaflet-control">
				<a class="leaflet-control-zoom-in" href="#" title="Zoom in" role="button" aria-label="Zoom in">+</a>
				<a class="leaflet-control-zoom-out" href="#" title="Zoom out" role="button" aria-label="Zoom out">−</a>
			</div>
		</div>
		<div class="leaflet-top leaflet-right"></div>
		<div class="leaflet-bottom leaflet-left"></div>
		<div class="leaflet-bottom leaflet-right">
			<div class="leaflet-control-attribution leaflet-control">
				<a href="https://leafletjs.com" title="A JS library for interactive maps">Leaflet</a>
				 | Map data © <a href="https://www.openstreetmap.org/">OpenStreetMap</a>
				  contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>
				  , Imagery © <a href="https://www.mapbox.com/">Mapbox</a>
			</div>
		</div>
	</div>
</div>

<script>

	var mymap = L.map('mapid').setView([46.8, 2], 6);

	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
		maxZoom: 18,
		minZoom: 4,
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
			'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
			'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		id: 'mapbox.streets'
	}).addTo(mymap);

</script>