<!DOCTYPE html>

<?php 

	include 'connexion.php';
	include 'jsonKey.php';
	include 'etablissementClass.php';


	function showInfoEtab($etablissement){

		if(isset($etablissement->uai))
			echo "- Code de l'etablissement : ".$etablissement->uai."<br>";
		if(isset($etablissement->typeEtab))
			echo "- Type d'etablissement : ".$etablissement->typeEtab."<br>";
		if(isset($etablissement->typoEtab))
			echo "- Typologie d'etablissement : ".$etablissement->typoEtab."<br>";
		if(isset($etablissement->nomEtab))
			echo "- Nom de l'etablissement : ".$etablissement->nomEtab."<br>";
		if(isset($etablissement->sigle))
			echo "- Sigle de l'etablissement : ".$etablissement->sigle."<br>";
		if(isset($etablissement->wikidata)) 
			echo "- Element wikidata de l'etablissement : <a href=".$etablissement->wikidata.">".$etablissement->wikidata."</a><br>";
		if(isset($etablissement->villeSiege))
			echo "- Commune du siege de l'etablissement : ".$etablissement->villeSiege."<br>";
		if(isset($etablissement->departSiege))
			echo "- Departement du siege de l'etablissement : ".$etablissement->departSiege."<br>";
		if(isset($etablissement->AcademieSiege))
			echo "- Academie du siege de l'etablissement : ".$etablissement->AcademieSiege."<br>";
		if(isset($etablissement->regSiege))
			echo "- Region du siege de l'etablissement : ".$etablissement->regSiege."<br>";
		if(isset($etablissement->adresse))
			echo "- Adresse de l'etablissement : ".$etablissement->adresse."<br>";
		if(isset($etablissement->codeP))
			echo "- Code postal de l'etablissement : ".$etablissement->codeP."<br>";
		if(isset($etablissement->pays))
			echo "- Pays de l'etablissement : ".$etablissement->pays."<br>";
		if(isset($etablissement->numTel))
			echo "- Numero de telephone de l'etablissement : ".$etablissement->numTel."<br>";

		echo '<br><br><br>';
		echo "Liste des formations de l'etablissement : <br>";


		$urlId = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=-1&refine.rentree_lib=2017-18&refine.etablissement=".$etablissement->uai;
		$jsonCodedId = file_get_contents($urlId.$varkey, true);
		$jsonId = json_decode($jsonCodedId, true);
	  	$var = $jsonId['records']['fields'];
	  	var_dump($var);
		foreach ($var as $value) {
			//$recordid = $value['recordid'];

			$intitule = $rec['fields']['libelle_intitule_1'];
			$niv = $rec['fields']['niveau_lib'];

			echo $intitule;
			//echo "<a href=formation.php?form=".$recordid.">".$intitule." (".$niv.")</a><br>";
		}
		echo "test";
	}


	if(isset($_GET['etab'])){
		$etab = $_GET['etab'];

		$url = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=1&refine.rentree_lib=2017-18&refine.etablissement=".$etab;
		$jsoncoded = file_get_contents($url.$varkey, true);
		$json = json_decode($jsoncoded, true);
		$rec = $json['records']['0']['fields'];



		$urlMap = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-etablissements-enseignement-superieur&rows=1&refine.uai=".$etab;
		$jsonMapcoded = file_get_contents($urlMap.$varkey, true);
		$jsonMap = json_decode($jsonMapcoded, true);
		$recMap = $jsonMap['records']['0']['fields'];


		$etablissement = new Etablissement($rec['etablissement'], $rec['etablissement_type2'], $rec['etablissement_type_lib'], $rec['etablissement_lib'], $rec['etablissement_sigle'], $rec['element_wikidata'], $rec['com_etab_lib'], $rec['dep_etab_lib'], $rec['aca_etab_lib'], $rec['reg_etab_lib'], $recMap['adresse_uai'], $recMap['com_code'], $recMap['pays_etranger_acheminement'], $recMap['numero_telephone_uai']);
	}

?>




<html lang="fr">


	<head>
		<meta charset="UTF-8"/>

		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="style.css">
		<title>Projet PHP</title>
		

	</head>


	<body>

		<div id="entete">
			<a href="home.php"><img src="logo.png" alt="Logo"></a>
			<h1>Choisis ton école</h1>
		</div>


		<?php
			showInfoEtab($etablissement);
		?>


	</body>


</html> 

