<!DOCTYPE html>


<?php
	include 'connexion.php';
	include 'jsonKey.php';

$url = 'https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=1000&sort=-rentree_lib&facet=rentree_lib&facet=etablissement_type2&facet=etablissement_type_lib&facet=etablissement&facet=etablissement_lib&facet=champ_statistique&facet=dn_de_lib&facet=cursus_lmd_lib&facet=diplome_rgp&facet=diplome_lib&facet=typ_diplome_lib&facet=diplom&facet=niveau_lib&facet=disciplines_selection&facet=gd_disciscipline_lib&facet=discipline_lib&facet=sect_disciplinaire_lib&facet=spec_dut_lib&facet=localisation_ins&facet=com_etab&facet=com_etab_lib&facet=uucr_etab&facet=uucr_etab_lib&facet=dep_etab&facet=dep_etab_lib&facet=aca_etab&facet=aca_etab_lib&facet=reg_etab&facet=reg_etab_lib&facet=com_ins&facet=com_ins_lib&facet=uucr_ins&facet=dep_ins&facet=dep_ins_lib&facet=aca_ins&facet=aca_ins_lib&facet=reg_ins&facet=reg_ins_lib&refine.rentree_lib=2017-18';

/*
	function facet($api, $name){
		$jsonCoded = file_get_contents($api.$varkey, true);
		$json = json_decode($jsonCoded, true);
	  	$var = $json['facet_groups'][0]['facets'];
	  	$list = array();
		foreach ($var as $value) {
			array_push($list, $value['name']);
		}
		sort($list);
		foreach ($list as $key => $value) {
			$val_name = str_replace(" ", "_", $value);
			echo "<div class=\"check\">";
				echo "<input type=\"checkbox\" id=\"";
				echo $val_name;
				echo "\" name=\"".$name."[]\" value=\"";
				echo $value;
				echo "\">";
				echo $value;
			echo "</div>";
		}	
	}
*/


	function facet($api, $name){
		$jsonCoded = file_get_contents($api.$varkey, true);
		$json = json_decode($jsonCoded, true);
	  	$var = $json['facet_groups'][0]['facets'];
		foreach ($var as $value) {
			echo "<div class=\"check\">";
				echo "<input type=\"checkbox\" id=\"";
				echo $value['name'];
				echo "\" name=\"".$name."[]\" value=\"";
				echo $value['name'];
				echo "\">";
				echo $value['name'];
			echo "</div>";
		}	
	}
	

$jsoncoded = file_get_contents($url.$varkey, true);
$json = json_decode($jsoncoded, true);

$active = array(0,0,0,0);
$filters = array(array(), array(), array(), array());

if (isset($_POST['reg'])) {
	$active[0] = 1;

	foreach($_POST['reg'] as $reg) {
		array_push($filters[0], $reg);
	}
}

if (isset($_POST['year'])) {
	$active[1] = 1;
	
	foreach($_POST['year'] as $year) {
		array_push($filters[1], $year);
	}
}

if (isset($_POST['study'])) {
	$active[2] = 1;

	foreach($_POST['study'] as $study) {
		array_push($filters[2], $study);
	}
}

if (isset($_POST['discipl'])) {
	$active[3] = 1;

	foreach($_POST['discipl'] as $discipl) {
		array_push($filters[3], $discipl);
	}
}


?>





<html lang="fr">


	<head>
		<meta charset="UTF-8"/>


		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="style.css">

		<link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css"
   		integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
   		crossorigin=""/>

   		<script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"
   		integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og=="
   		crossorigin=""></script>


		<title>Projet PHP</title>
		

	</head>


	<body>

		<div id="entete">
			<a href="home.php"><img src="logo.png" alt="Logo"></a>
			<h1>Choisis ton école</h1>
		</div>



	    <form method="POST" action="home.php">
		    <div class="middle">
		      <div class="menu">

		      <menu>

		        <li class="item" id="region">
		          <a href="#region" class="btn">Regions</a>
		          <div class="smenu">
					  <fieldset>
					  	<?php facet("https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&sort=-rentree_lib&facet=reg_etab_lib", "reg"); ?>
					  </fieldset>
		          </div>
		        </li>



		        <li class="item" id="annee">
		          <a href="#annee" class="btn">Années d'étude</a>
		          <div class="smenu">
					  <fieldset>
					  	<?php facet('https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&sort=-rentree_lib&facet=niveau_lib', 'year'); ?>
					  </fieldset>
		          </div>
		        </li>



		        <li class="item" id="etude">
		          <a href="#etude" class="btn">Type d'études</a>
		          <div class="smenu">
					  <fieldset>
					  	<?php facet('https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&sort=-rentree_lib&facet=diplome_rgp', 'study'); ?>
					  </fieldset>
		          </div>
		        </li>



		        <li class="item" id="discipline">
		          <a href="#discipline" class="btn">Disciplines</a>
		          <div class="smenu">
					  <fieldset>
						<?php facet('https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&sort=-rentree_lib&facet=sect_disciplinaire_lib', 'discipl'); ?>
					  </fieldset>
		          </div>
		        </li>

		        <li class="item" id="rechercher">
		          	<input type="submit" class="btn">
		        </li>

		       </menu>
		      </div>
		    </div>
		</form>

		<div class="map">
			<?php include('map.php');?>
		</div>

		<?php
		$nbResult = 0;
		$jsonCodedMap = file_get_contents('https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-etablissements-enseignement-superieur&rows=323&sort=uo_lib&facet=uai&facet=type_d_etablissement&facet=com_nom&facet=dep_nom&facet=aca_nom&facet=reg_nom&facet=pays_etranger_acheminement'.$varkey, true);
		$jsonMap = json_decode($jsonCodedMap, true);
	  	$varMap = $jsonMap['records'];

	  	$varrec = $json['records'];
		$i = 0;	  	
		foreach ($varMap as $valueMap) {
			foreach ($varrec as $val) {
				$test = array(0,0,0,0);

				if(isset($valueMap['fields']['coordonnees']) && $valueMap['fields']['uai'] == $val['fields']['etablissement']){

					if($active[0] == 1){
						if(in_array($val['fields']['reg_etab_lib'], $filters[0])){
							$test[0] = 1;
						}
					}

					if($active[1] == 1){
						if(in_array($val['fields']['niveau_lib'], $filters[1])){
							$test[1] = 1;
						}
					}

					if($active[2] == 1){
						if(in_array($val['fields']['diplome_rgp'], $filters[2])){
							$test[2] = 1;
						}
					}

					if($active[3] == 1){
						if(in_array($val['fields']['sect_disciplinaire_lib'], $filters[3])){
							$test[3] = 1;
						}
					}


					if($test == $active){
						echo "<script>";
						echo "var marker".$i." = L.marker([".$valueMap['fields']['coordonnees'][0].", ".$valueMap['fields']['coordonnees'][1]."]).addTo(mymap);";
						if(isset($valueMap['fields']['url'])) {
							
							$sql = "SELECT * FROM `ClicCounter` WHERE `link` = '".$valueMap['fields']['url']."';";
							$req = $bdd->query($sql);
							$row = $req->fetch();
							
							if ($row == false) {
								$clics = 0;
							} else {
								$clics = $row['nbClic'];
							}

							echo "marker".$i.".bindPopup(\"<b>".$valueMap['fields']['uo_lib']."</b><br><a href=etablissement.php?etab=".$val['fields']['etablissement'].">Page d'informations</a><br><br><a href=redirect.php?link=".$valueMap['fields']['url'].">Site web </a><i>(cliqué ".$clics." fois)</i>\");";

						} else{
							echo "marker".$i.".bindPopup(\"<b>".$valueMap['fields']['uo_lib']."</b>\");";
						}
						echo "</script>";
						$nbResult = $nbResult + 1;

					}
				}
			}
		}
		echo $nbResult." resultats";
		?>

	<footer>
		<a href='https://bitbucket.org/kevingomes/php-v1'>Pour en savoir plus</a>
	</footer>
	</body>


</html> 

