<!DOCTYPE html>

<?php 

	include 'connexion.php';
	include 'jsonKey.php';
	include 'formationClass.php';


	function showInfoForm($formation){
		echo "- Code de l'etablissement : ".$formation->uai."<br>";

		echo "- Nom de l'etablissement : ".$formation->nomEtab."<br>";

		echo "- <a href=etablissement.php?etab=".$formation->uai.">Page d'information de l'etablissement</a><br>";

		echo "- Type de diplome : ".$formation->typeDipl."<br>";

		echo "- Cycle universitaire (LMD) : ".$formation->cycleUniv."<br>";

		echo "- Regroupement de diplomes : ".$formation->regroupDipl."<br>";

		echo "- Diplome : ".$formation->dipl."<br>";

		echo "- Type de diplome ou de formation : ".$formation->typeDiplForm."<br>";

		echo "- Libellé du diplôme ou de la formation : ".$formation->libDipl."<br>";

		echo "- Niveau dans le diplome : ".$formation->nivDipl."<br>";

		echo "- Grande discipline : ".$formation->grandDisc."<br>";

		echo "- Discipline : ".$formation->disc."<br>";

		echo "- Secteur disciplinaire : ".$formation->sectDisc."<br>";

		echo "- Nombre d'étudiants inscrits : ".$formation->nbEtudi."<br>";

		echo "- Dont hommes : ".$formation->nbHomme."<br>";

		echo "- Dont femmes : ".$formation->nbFemme."<br>";
	}



	if(isset($_GET['form'])){
		$id = $_GET['form'];



		$link = "formation.php?form=".$id;
		$sql = "SELECT * FROM `ClicCounter` WHERE `link` = '".$link."';";
		$req = $bdd->query($sql);
		$row = $req->fetch();

		if ($row == false) {
			$requete = "INSERT INTO `ClicCounter`(`link`, `nbClic`, `isFormation`) VALUES ('".$link."',1 ,1);";
		} else {
			$requete = "UPDATE `ClicCounter` SET `nbClic` = `nbClic`+1 WHERE `link` = '".$link."';";
		}

		$result = $bdd->query($requete);
		$req->closeCursor();
		$result->closeCursor();

		



		$url = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=-1&sort=-rentree_lib&refine.rentree_lib=2017-18&refine.recordid=".$id;
		$jsoncoded = file_get_contents($url.$varkey, true);
		$json = json_decode($jsoncoded, true);
		$rec = $json['records']['0']['fields'];

		$formation = new Formation($rec['etablissement'], $rec['etablissement_lib'], $rec['dn_de_lib'], $rec['cursus_lmd_lib'], $rec['diplome_rgp'], $rec['diplome_lib'], $rec['typ_diplome_lib'], $rec['libelle_intitule_1'], $rec['niveau_lib'], $rec['gd_disciscipline_lib'], $rec['discipline_lib'], $rec['sect_disciplinaire_lib'], $rec['effectif'], $rec['hommes'], $rec['femmes']);
	}

?>




<html lang="fr">


	<head>
		<meta charset="UTF-8"/>

		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="style.css">
		<title>Projet PHP</title>
		

	</head>


	<body>

		<div id="entete">
			<a href="home.php"><img src="logo.png" alt="Logo"></a>
			<h1>Choisis ton école</h1>
		</div>


		<?php
			showInfoForm($formation);
		?>


	</body>


</html> 

