<?php

class Etablissement  {
	//JSON Ecoles
	public $uai; //etablissement
	public $typeEtab; //etablissement_type2
	public $typoEtab; //etablissement_type_lib
	public $nomEtab; //etablissement_lib
	public $sigle; //etablissement_sigle
	public $wikidata; //element_wikidata
	public $villeSiege; //com_etab_lib
	public $departSiege; //dep_etab_lib
	public $AcademieSiege; //aca_etab_lib
	public $regSiege; //reg_etab_lib
	//JSON Map
	public $adresse; //adresse_uai
	public $codeP; //com_code
	public $pays; //pays_etranger_acheminement
	public $numTel; //numero_telephone_uai

	public function __construct($uai, $typeEtab, $typoEtab, $nomEtab, $sigle, $wikidata, 
		$villeSiege, $departSiege, $AcademieSiege, $regSiege, $adresse, $codeP, $pays, $numTel) {
		
		$this->uai = $uai; 
		$this->typeEtab = $typeEtab;
		$this->typoEtab = $typoEtab;
		$this->nomEtab = $nomEtab;
		$this->sigle = $sigle;
		$this->wikidata = $wikidata;
		$this->villeSiege = $villeSiege;
		$this->departSiege = $departSiege;
		$this->AcademieSiege = $AcademieSiege;
		$this->regSiege = $regSiege;
		$this->adresse = $adresse;
		$this->codeP = $codeP;
		$this->pays = $pays;
		$this->numTel = $numTel;
	} 
}

?>